# dh-cargo fork

This is a slight fork of the debhelper script [dh-cargo],
with the following functional differences
since git commit 5cc7f7b
(included with version 28 released 2021-11-07):

  * support custom dh option --sourcedirectory
  * generate cargo-checksum during install
  * omit installing any .git* files or directories
  * omit installing license files
  * omit installing debian/patches
  * use lockfile when available, updated without checksums,
    or instead use debian/Cargo.lock if that is also available
  * use crates embedded below debian/vendorlibs when available
  * build binaries in target dh_auto_build (not dh_auto_test)
  * run tests in target dh_auto_test (not just do a test build)

Also included is a slight fork of related cargo wrapper script,
with the following functional differences
since git commit 7823074
(included with version 0.57.0-6 released 2022-04-10):

  * fix support relative path in CARGO_HOME, as documented
  * support DEB_BUILD_OPTIONS=terse
  * use optimized release or bench profiles by default;
    support DEB_BUILD_OPTIONS=noopt to use dev or test profiles

[dh-cargo]: <https://salsa.debian.org/rust-team/dh-cargo/-/blob/master/cargo.pm>

[cargo]: <https://salsa.debian.org/rust-team/cargo/-/blob/debian/sid/debian/bin/cargo>


## Usage

In your source package,
copy directory `dh-cargo` to `debian/dh-cargo`
and edit `debian/rules` to something like this:

```
#!/usr/bin/make -f

# use local fork of dh-cargo and cargo wrapper
PATH := $(CURDIR)/debian/dh-cargo/bin:$(PATH)
PERL5LIB = $(CURDIR)/debian/dh-cargo/lib
export PATH PERL5LIB

%:
	dh $@ --buildsystem cargo --sourcedirectory=rustls
```


 -- Jonas Smedegaard <dr@jones.dk>  Fri, 24 Jun 2022 02:40:40 +0200
